#ifdef TEST_7

/*
 * Checks to see if Periodic tasks can be created with a WCET > Period.
 * Should result in an error state.
 */

#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>

#include "../os.h"
#include "../usart.h"

void periodic(){
	int i;
	for(i=0;i<5;i++){
		_delay_ms(25);
		Task_Next();
	}
}

extern int r_main(){    
    usart_init();

    Task_Create_Periodic(periodic,1, 2, 3, 0);

    return 0;
}

#endif