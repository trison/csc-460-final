#ifdef TEST_6

/*
 * Checks to see if Periodic, System and RR tasks can be created, and if they run in the expected order.
 * Prints out a trace of functions.
 */

#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>

#include "../os.h"
#include "../usart.h"

int trace[17];
int trace_index = 0;

void check_trace(){
	int i;
	for(i=0;i<17;i++){
		usart_send((char)((int)'0' + trace[i]));
	}
}

void periodic1(){
	int i;
	for(i=0;i<5;i++){
		trace[trace_index++] = 1;
		Task_Next();
	}
	if(trace_index >= 17){
		Task_Create_RR(check_trace,0);
	}
}

void periodic2(){
	int i;
	for(i=0;i<5;i++){
		trace[trace_index++] = 2;
		Task_Next();
	}
	if(trace_index >= 17){
		Task_Create_RR(check_trace,0);
	}
}

void rr(){
	int i;
	for(i=0;i<5;i++){
		//_delay_ms(5);
		trace[trace_index++] = 3;
		Task_Next();
	}
	if(trace_index >= 17){
		Task_Create_RR(check_trace,0);
	}
}

extern int r_main(){    
    usart_init();

    trace[trace_index++] = 0;
    Task_Create_Periodic(periodic1,1, 2, 1, 0);
	Task_Create_Periodic(periodic2,1, 2, 1, 3);
	Task_Create_RR(rr,1);
	trace[trace_index++] = 0;

    return 0;
}

#endif