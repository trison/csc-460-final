#ifdef TEST_13

/*
 * Interrupt publishes to service
 */

#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>

#include "../os.h"
#include "../usart.h"

int trace[8];
int trace_index = 0;
SERVICE *service;

void check_trace(){
	int key[] = {0,0,1,2,2,2,2,2};
	int i;
	for(i=0;i<8;i++){
		if(trace[i] != key[i]){
			usart_send('F');
		}
		//usart_send((char)((int)'0' + trace[i]));//
	}
	usart_send('C');
}

void setup() {

  TCCR3A = 0;
  TCCR3B = 0;  
  //Set to CTC (mode 4)
  TCCR3B |= (1<<WGM32);
  
  //Set prescaller to 256
  TCCR3B |= (1<<CS32);
  
  //Set TOP value (0.5 seconds)
  OCR3A = 31250;
  
  //Enable interupt A for timer 3.
  TIMSK3 |= (1<<OCIE3A);
  
  //Set timer to 0 (optional here).
  TCNT3 = 0;
}

ISR(TIMER3_COMPA_vect){
	trace[trace_index++] = 1;
	//usart_send('!');
  	Service_Publish(service, 10);
}

void bar(){
	int i;
	int16_t val;
	Service_Subscribe(service, &val);
	for(i=0;i<5;i++){
		trace[trace_index++] = 2;
		Task_Next();
	}
	check_trace();
}

extern int r_main(){    
    usart_init();
    setup();
    service = Service_Init();

    trace[trace_index++] = 0;
    Task_Create_System(bar, 1);
	trace[trace_index++] = 0;

    return 0;
}

#endif