#ifdef TEST_4

/*
 * Checks to see if Periodic tasks can be created, and if they run in the expected order.
 * Prints 'F' if tasks execute in the wrong order. Prints 'C' when complete.
 */

#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>

#include "../os.h"
#include "../usart.h"

int trace[15];
int trace_index = 0;

void check_trace(){
	int key[] = {1,2,1,1,2,1,1,2,1,1,2,1,1,2,1};
	int i;
	for(i=0;i<15;i++){
		if(trace[i] != key[i]){
			usart_send('F');
		}
	}
	usart_send('C');
}

void periodic1(){
	int i;
	for(i=0;i<10;i++){
		trace[trace_index++] = 1;
		//usart_send('1');
		Task_Next();
	}
}

void periodic2(){
	int i;
	for(i=0;i<5;i++){
		trace[trace_index++] = 2;
		//usart_send('2');
		Task_Next();
	}
	Task_Create_RR(check_trace,0);
}

extern int r_main(){    
    usart_init();

    Task_Create_Periodic(periodic1,1, 5, 2, 2);
	Task_Create_Periodic(periodic2,1, 10, 2, 4);

    return 0;
}

#endif